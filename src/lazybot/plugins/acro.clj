(ns lazybot.plugins.acro
  (:use lazybot.registry
        [lazybot.utilities :only [prefix]]))

;; contributed by J. Andrew Thompson 2013-08-22
;
(def dict
  {"A" ["Abicus" "Abismal"]
   "B" ["Birthlube" "Byzantine" "Bacon"]
   "C" ["Children" "Crappy" "Cynical"]
   "D" ["Damn" "Danish" "Draconian" "Department"]
   "E" ["Emo" "Elbow Grease" "Elvish"]
   "F" ["Fandango" "Freakin" "Festivus"]
   "G" ["Glassfish" "Gangster" "Great"]
   "H" ["Hella" "Hood ride" ]
   "I" ["Irksome" "Interweb" "Illinois"]
   "J" ["Jolly" "Java" "Jawa" ]
   "K" ["K-Mart" "Kwanzaa" "Kevlar" ""]
   "L" ["Liquid" "Lucky" "Lothlorien" ]
   "M" ["Meat" "Mega" "Manhood"]
   "N" ["Neptha" "Ninja" "NSA"]
   "O" ["Oprah" "Ostracised" "One"]
   "P" ["Putrid" "Panzy" "Party"]
   "Q" ["Qwerty" "Quigley" "Quenya"]
   "R" ["Random" "Round" "Righteous"]
   "S" ["Stupid" "Steak" "Swag"]
   "T" ["Turd" "Toasty" "Titillating" ]
   "U" ["Ubiquitous" "Userper"]
   "V" ["Verily" "Venomous"]
   "W" ["Wanky" "Wonderful" ]
   "X" ["Xenomorphed"]
   "Y" ["Young" "Y'all" "Yeller" "You're"]
   "Z" ["Zombie" "Zygote"]

   }
  )

(defn get-acro [acro]
  (clojure.string/join " "
                       (for [x acro]
                              (rand-nth (get dict (.toUpperCase (str x))))
                              )))
(defplugin
  (:cmd
   "Get the definition for an acronymn."
   #{"acro" "whatsa" "define"}
   (fn [{:keys [bot nick channel args] :as com-m}]
     (send-message com-m (prefix nick (get-acro (first args)))))))


