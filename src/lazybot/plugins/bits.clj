(ns lazybot.plugins.bits
  (:use lazybot.registry)
  (:use [lazybot.utilities :only [prefix]])
  (:require [clojure.string :as s]))

; plugins for bitsbot, for use in #bitswebteam

; (02:02:06 PM) sysbajesse: also it would be cool if it was time dependent,
; like if I said good evening and it was before noon he should have a witty
; response.

; would also like a rate limiter
; let's keep a hash of usernames to last-greeted times
(def GREETING-TIMEOUT (* 60 60)) ; one hour (in seconds)
(defonce greeted-lately (atom {}))

(defn now
  "current time in seconds"
  []
  (/ (System/currentTimeMillis) 1000.0))

(defn note-greeting [nick]
  (swap! greeted-lately assoc nick (now)))

(defn can-greet? [nick]
  (if-let [last-time (get @greeted-lately nick)]
    (> (- (now) last-time) GREETING-TIMEOUT)
    true)) ; no record for this nick, so it's fine

; we could evict out any nicks whose timeout has expired but we can be fairly
; sure that  the list will never get very large in #bitswebteam as we have a
; small population, and each new greeting will simply replace an old greeting
; record.


(def morning-patterns
  [#"(?i).*good.morning.*"
   #"(?i).*top.*o.*morning.*"
   ])

(def morning-sayings
  ["Good Morning!"   ; poor man's weighted choice ;-)
   "Good Morning!"
   "Morning!"
   "Morning!"
   "Top o' the mornin' to ye!"
   "Top o' the mornin' to ye!"
   "Guten Morgen!"
   "Доброе утро, товарищи! (Good morning, comrades!)"
   "yes. morning. good one. have it."
   "InsufficientCoffeeException: require more caffeine before I can return your morning greeting"
   ])


(def evening-patterns
  [#"(?i).*good.evening.*"
   #"(?i).*good.night.*"
   #"(?i).*goodnight.*"
   #"(?i).*g'night.*"
   ])

(def evening-sayings
  ["you too!"
   "g'night!"
   "good night!"
   "have a good evening!"
   "you have a good one too!"
   ])

(def the-crow-says
  ["caw"
   "caw?"
   "caw!"
  ])

(defn reply-for-patterns
  [message patterns replies]
  (if (some #(re-matches % message) patterns)
    (rand-nth replies)
    nil))

(defplugin
  (:cmd
    "a morning greeting"
    #{"morning"}
    (fn [{:keys [args] :as com-m}]
      (send-message com-m (rand-nth morning-sayings))))

  (:cmd "deny requests for a nap"
      #{"naptime"}
      (fn [{:keys [nick] :as com-m }]
        (send-message com-m (str nick ": You wish, buddy."))))

  (:cmd "respond to calls for murder"
    #{"murder"}
    (fn [ { :keys [args] :as com-m } ]
      (send-message com-m (rand-nth the-crow-says) )
    ))

  (:cmd "reassurance in a time of need"
      #{"panic"}
      (fn [{:keys [nick] :as com-m }]
        (send-message com-m (str nick ": Don't worry, it's all going to be okay."))))

  ; detect people saying good morning/goodnight and reply to them
  (:hook
    :on-message
    (fn [{:keys [nick message] :as com-m}]
      (when-let [saying (or (reply-for-patterns message morning-patterns morning-sayings)
                            (reply-for-patterns message evening-patterns evening-sayings)
                            )]
        (when (can-greet? nick)
          (send-message com-m (prefix nick saying))
          (note-greeting nick)))))
  )

; also: look for non-head @8ball?
(comment
  ; https://github.com/clojure/data.codec
  (require '[clojure.data.codec.base64 :as b64])

  )

