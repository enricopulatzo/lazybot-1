(ns lazybot.plugins.bittwiddlers
  (:require [clojure.string :as string])
  (:use [lazybot registry]
        [lazybot.utilities]
        ;; [clojure.data.json :only [read-json json-str]]
        )
  (:require [clojure.data.json :as json])
  (:require [clj-http.client :as client])
  (:import org.apache.commons.lang.StringEscapeUtils)
  )

(def BASE-URL "https://i.bittwiddlers.org/api/v1/")
(def LISTING-PATH "list/bitsbot")
(def ADD-PATH "add/bitsbot")
(def DELETE-PATH "delete/")
(def GALLERY-URL "http://i.bittwiddlers.org/col/only/bitsbot?all")

;; TODO: stick those credentials in the config file, read 'em later
;; TODO: import his self-signed CA cert
(def HTTP-OPTIONS
  {:basic-auth ["bitsbot" "It's a secret to everybody."]
   :insecure? true
   ;; :throw-exceptions false
   })

(comment
;; (05:25:29 PM) Jim Dunne: POST to /api/v1/add/bitsbot:
;; {
;;   "sourceURL": "http://i.bittwiddlers.org/KQT.gif",
;;   "submitter": "nathan",
;;   "title": "Bitsbot Test 2"
;; }

;; {
;;   "sourceURL": "http://media.giphy.com/media/OCu7zWojqFA1W/giphy.gif",
;;   "submitter": "nathanic",
;;   "title": "Hold on to your butts! (Samuel L. Jackson / Jurassic Park)",
;;   "isClean": "true"
;; }

  (random-gif!)
  (filter (fn [gif] (= (:kind gif) "youtube")) (gif-list!))
  (post-gif! "nathanic" "Life finds a way. (Jurassic Park)" "http://media.giphy.com/media/VHW0X0GEQQjiU/giphy.gif" true)
  (post-gif! "nathanic" "Denholm Renholm is done with this." "https://i.imgur.com/FTC00ct.jpg" true)

  (post-gif! "nathanic" "DISAPPOINTED!!!" "https://i.imgur.com/yLjgqnB.gif" true)
  (post-gif! "nathanic" "Success! (Space Jump)" "http://i.minus.com/iF94rRtTfujf8.gif" true)
  (post-gif! "nathanic" "Fuck no (LotR)" "http://i.minus.com/iF94rRtTfujf8.gif" false)
  (post-gif! "nathanic" "Bill Cosby brandishing his magic wiener" "https://i.imgur.com/CDLKIxF.gif" true)
  (post-gif! "nathanic" "Bilbo double flip-off" "https://i.imgur.com/SnYtXcy.gif" false)

  (def delete-response (delete-gif! "KOw"))
  (println delete-response)

  (-> (api-get LISTING-PATH)
      :body
      ;; json/read-str
      read-json
      (get "result")
      )
  (keyword "what about spaces?")

  (client/post BASE-URL
               {:body "just a test, this won't auth anyway"
                :insecure? true
                :throw-exceptions false
                })
  (try
    (-> (post-gif! "tester" "test; please delete" "http://example.com/foo.gif" true)
        :body
        read-json
        )
    (catch Exception e
      (println "got exception: " e)
      ))

  )

; this is basically an adapter between current clojure.data.json api and 1.x that i had been using
(defn read-json [s]
  (json/read-str s :key-fn keyword))

(defn api-get [req-path]
  (client/get (str BASE-URL req-path) HTTP-OPTIONS))

; TODO: parse response of posts
(defn api-post [req-path payload]
  (client/post (str BASE-URL req-path)
               (assoc HTTP-OPTIONS
                      :body (json/write-str payload)
                      :content-type :json
                      :throw-exceptions false
                      )))

(defn url-from-id [base62id]
  (str "http://i.bittwiddlers.org/b/" base62id))

(defn is-ok? [response]
  (and (>= 200 (:status response)) (< 300)))

(defn post-gif! [nick title url clean-flag] ; pass :clean if isClean=true, otherwise isClean=false
  (let [response  (api-post ADD-PATH
                            {:sourceURL url
                             :title title
                             :isClean (= clean-flag :clean)
                             :submitter nick
                             })]
    (if (is-ok? response)
      {:ok? true
       :url (-> response
                :body
                read-json
                :result
                :base62id
                url-from-id
                )}
      {:ok? false
       :error (-> response
                  :body
                  read-json
                  :error
                  )})))

(defn delete-gif! [base62id]
  (api-post (str DELETE-PATH base62id) HTTP-OPTIONS))

(defn gif-list!
  []
  (-> (api-get LISTING-PATH)
      :body
      read-json
      :result
      :list
      ))

(defn random-gif!
  "pull a random gif from bittwiddlers; returns a hashmap of gif info"
  []
  (->> (gif-list!)
      (filter (fn [{:keys [kind]}] (not= kind "youtube")))
      rand-nth
      ))

(comment
  (seq (search-gifs-regex (prepare-regex "evil")))
  (def query #"(?i).*evi.*laugh.*")
  (def query #"evi.*laugh")
  (def title "Dr. Evil Laughing")
  (re-matches (prepare-regex query) title)

  (filter (fn [{:keys [title]}] (re-matches query title)) [{:title "yo"} {:title "Dr. Evil Laughing"}])
  )

(defn prepare-regex [re-str]
  (re-pattern (str "(?i).*" re-str ".*")))

; probably also want a non-regex version
(defn search-gifs-regex
  "search for a gif with a given string match"
  [query]
  (->> (gif-list!)
       ; TODO: filter for isClean=true?
       (filter (fn [{:keys [title]}] (re-matches query title)))
       (filter (fn [{:keys [kind]}] (not= kind "youtube")))
       ))

; consider just looking for each word to be present, or punting to some search expression lib
(defn search-gifs-substring
  "search for a gif with a given string match"
  [substring]
  (->> (gif-list!)
       (filter (fn [{:keys [title]}] (.contains (.toLowerCase title) (.toLowerCase substring))))
       (filter (fn [{:keys [kind]}] (not= kind "youtube")))
       ))

(defn safe-rand-nth [coll]
  (if (empty? coll)
    nil
    (rand-nth coll)))

(defn seems-like-url? [s]
  (some? (re-matches #"(?i)http[s]?://.*" s)))

(defn seems-like-ip? [s]
  (some? (re-matches #"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" s)))

; TODO: local flagging or blacklisting system for racist etc. gifs?

(comment
  (describe-gif (random-gif!))
  (describe-gif (first (search-gifs-substring "butts")))
  (describe-gif (first (search-gifs-substring "fucky")))
  (describe-gif
    {:title "title goes here"
     :isClean false
     :base62id "foo"
     :submitter "42.1.1.2"
     })
  (seems-like-url? "http://foo.com")
  (seems-like-url? "https://foo.com")
  (seems-like-url? "hTtPs://foo.com")
  (seems-like-url? "foo.com")
  (re-matches #"(?i)(?i)foo" "foO")
  (string/join ":" ["hi" "there" "bub"])
  (seems-like-ip? "42.1.1.2")
  (seems-like-ip? "42..1.2")
  (seems-like-ip? "42!1!1!2")
  (seems-like-ip? "42.1.441.233") ; heh
  (seems-like-ip? "apples.moneys.bumnuggets.fritters")
  (seems-like-ip? "aa.bbb.cc.dd")
  (seems-like-ip? "000000000")
)

; TODO: support youtube results
(defn describe-gif [{:keys [title base62id submitter isClean]}]
  (str (when-not isClean "[NSFW?] ")
       "`" title "` "
       (url-from-id base62id)
       (when (and submitter (not (seems-like-ip? submitter)) )
         (str " [submitted by " submitter "]"))))

(defplugin
  (:cmd
    "show a random gif from bittwiddlers.org.  no guarantees expressed or implied!"
    #{"randomgif" "random-gif"}
    (fn [com-m]
      (send-message com-m (describe-gif (random-gif!)))))

  (:cmd
    "Search for a gif on bittwiddlers whose title contains the given string. Case-insensitive."
    #{"findgif" "find-gif" "react"}
    (fn [{:keys [args nick] :as com-m}]
      (if-let [gif (safe-rand-nth (search-gifs-substring (string/join " " args)))]
        (send-message com-m (prefix nick (describe-gif gif)))
        (send-message com-m (prefix nick "I can't find any gifs for that search string, so you should google me up one and @addgif it!"))
        )))

  (:cmd
    "Show all gifs that match a search string. Case-insensitive. Might be spammy."
    #{"findallgifs" "find-all-gifs"}
    (fn [{:keys [args nick] :as com-m}]
      (if-let [gifs (seq (search-gifs-substring (string/join " " args)))]
        (doseq [gif gifs]
          (send-message com-m (prefix nick (describe-gif gif))))
        (send-message com-m (prefix nick "I can't find any gifs for that search string, so find some and @addgif them!"))
        )))

  (:cmd
    "Search for a gif on bittwiddlers.org via regex search on the image titles. Case-insensitive."
    #{"findgif~" "refindgif-re" "find-gif-regex"}
    (fn [{:keys [args nick] :as com-m}]
      (if-let [gif (safe-rand-nth (search-gifs-regex (prepare-regex (string/join " " args))))]
        (send-message com-m (prefix nick (describe-gif gif)))
        (send-message com-m (prefix nick "I can't find any gifs for that search string :-("))
        )))

  (:cmd
    "Show all gifs that match a regex."
    #{"findallgifs~" "find-all-gifs~"}
    (fn [{:keys [args nick] :as com-m}]
      (if-let [gifs (seq (search-gifs-regex (prepare-regex (string/join " " args))))]
        (doseq [gif gifs]
          (send-message com-m (prefix nick (describe-gif gif))))
        (send-message com-m (prefix nick "I can't find any gifs for that search string :-("))
        )))

  (:cmd
    "See a webpage with all the gifs we've uploaded to i.bittwiddlers.org, an image collection site run by Nathan's friend Jim."
    #{"ourgifs" "our-gifs" "bitsbot-gifs"}
    (fn [{:keys [nick] :as com-m}]
      (send-message com-m (prefix nick "All the gifs we've uploaded are here: " GALLERY-URL))
      ))

  (:cmd
    "Upload a new image to bittwiddlers. If it contains profanity or worse, it'd be nice if you used `@addgif!` instead. Usage: @addgif <image-url> <rest of line is title text>"
    #{"addgif" "add-gif"}
    (fn [{:keys [args nick] :as com-m}]
      (if (or (< (count args) 2) (not (seems-like-url? (first args))))
        (send-message com-m (prefix nick "Usage: @addgif <image-url> <rest of line is title text>"))
        (let [[url & title-pieces] args
              title                (string/join " " title-pieces)
              response             (post-gif! nick title url :clean)
              ]
          (if (:ok? response) 
            (send-message com-m (prefix nick "Uploaded! " (:url response)))
            (send-message com-m (prefix nick "Dang it, got an error: `" (:error response) "`"))
            )))))

  (:cmd
    "Upload a new image to bittwiddlers that you think is maybe a bit sketchy, even if just for language. Usage: @addgif! <image-url> <rest of line is title text>"
    #{"addgif!" "add-gif!"}
    (fn [{:keys [args nick] :as com-m}]
      (if (or (< (count args) 2) (not (seems-like-url? (first args))))
        (send-message com-m (prefix nick "Usage: @addgif <image-url> <rest of line is title text>"))
        (let [[url & title-pieces] args
              title                (string/join " " title-pieces)
              response             (post-gif! nick title url :dirty)
              ]
          (if (:ok? response) 
            (send-message com-m (prefix nick "Uploaded! " (:url response)))
            (send-message com-m (prefix nick "Dang it, got an error: `" (:error response) "`"))
            )))(if (and (>= (count args) 2) (seems-like-url? (first args)))

                 (send-message com-m (prefix nick "Usage: @addgif! <image-url> <rest of line is title text>"))
                 )))
  )

