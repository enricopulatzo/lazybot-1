(ns lazybot.plugins.ddate
  (:use lazybot.registry)
  (:use [clojure.java.shell :only [sh]])
  )

(defplugin
  (:cmd
   "Find out today's date in the Discordian Calendar"
   #{"ddate"}
   (fn [{:keys [bot args] :as com-m}]
     (send-message
      com-m
       (-> (sh "ddate")
           :out
           (.replace "\n" ". ")
           ))))
  )
