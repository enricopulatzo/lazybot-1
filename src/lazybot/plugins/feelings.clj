(ns lazybot.plugins.feelings
  (:require [clojure.string :as string])
  (:use [lazybot registry]
        [somnium.congomongo :only [fetch fetch-one insert! destroy! update!]]))

; feelings.clj: a lazybot plugin to remember and report how people feel about various stuff

; TODO: parameterize prefix instead of literal '@', or don't bother?

(comment
  ; one-liner to reload all bitsbot plugins
  (apply lazybot.core/reload-all (vals @lazybot.core/bots))
  )

; this was empirically determined
(def MAX-MSG-LEN 400)

;; not used anymore, but could be useful elsewhere
;; though it has a bug where it doesn't handle the case of the splitting
;; sequence being too rare.
#_(defn break-up-long-message
  "break up a string into a seq of strings, each no longer than `len`,
  optionally splitting on `split-on` (defaults to \"\")."
  ([s len]
   (break-up-long-message s len " "))
  ([s len split-on]
   (loop [m 0, n len, acc []]
     (let [n      (min n (count s))
           endpos (if (= n (count s))
                    n
                    (.lastIndexOf s split-on n))
           endpos (if (pos? endpos)
                    endpos
                    (count s))
           seg    (subs s m endpos)
           ]
       (if (< n (count s))
         (recur (inc endpos)
                (+ endpos len)
                (conj acc seg))
         (conj acc seg))))))

#_(defn send-long-message [com-m msg]
  (doseq [seg (break-up-long-message msg MAX-MSG-LEN )]
    (send-message com-m seg)))

(defn sum
  [nums]
  (if (empty? nums)
    0
    (apply + nums)))

(defn- interposed-len
  "given a collection of strings and a separator, figure out their total length
  when concatenated with the separator interposed"
  [coll sep]
  (+ (sum (map count coll))
     (* (dec (count coll)) (count sep))))

(defn- interpose-messages
  "append a separator to each string in a collection, except the last one."
  [messages sep]
  (conj (mapv #(str % sep) (butlast messages))
        (last messages)))


(defn format-item-list
  "format a list of items (strings) into a list of messages, where the items
  have commas interposed, and no individual message is greater than `len`.
  Unless an individual item is longer than `len`, in which case it is left as
  its own message."
  [items msg-len]
  (loop [messages [], acc [], remaining items]
    (let [sep      ", "
          new-item (first remaining)
          new-len  (+ (interposed-len acc sep) (count sep) (count new-item))
          ]
      (cond
        ; no items remaining -> we're done
        (or (nil? new-item) (empty? remaining))
        (interpose-messages
          (conj messages (apply str (interpose sep acc)))
          sep)

        ; no accumulator, and current item is itself too long
        ; just make that item a message of its own
        ; TODO: maybe break it up and shove the rest back into `remaining`
        (and (empty? acc)
             (> (count new-item) msg-len))
        (recur (conj messages new-item)
               []
               (rest remaining)
               )

        ; accumulator + new item is too long:
        ; emit accumulator
        (> new-len msg-len)
        (recur (conj messages (apply str (interpose sep acc)))
               []
               remaining)

        :else
        (recur messages
               (conj acc new-item)
               (rest remaining)
               )))
    ))


(defn send-item-list [com-m prefix items]
  (doseq [msg (format-item-list items (- MAX-MSG-LEN (count prefix)))]
    (send-message com-m (str prefix msg))))


(def love-responses
  ["Okay."
   "Your @love has been noted."
   "That's beautiful."
   "I could never forget a @love like yours."
   "All you need is @love."
   "Spread the @love! Then, query the love with @feelings <person> or @feelings-about <thing>."
   "Whole lotta @love!"
   "@love don't come easy, you just have to wait (several milliseconds)."
   ])

(def hate-responses
  ["Sure."
   "I'll bet you do."
   "Your hatred is now inscribed on my hard drive for all time (or until the the disk fails)."
   "Good, goooood... Let the @hate flow through you."
   "Fear leads to anger, anger leads to hate, and @hate leads to a record in my database."
   "Wow, okay then."
   "Okay.  Does anyone else here feel the same way?"
   ])

(def meh-responses
  ["Whatever, man."
   "Lack of opinion noted."
   "Cool."
   "Uh-huh."
   "Luckily for you, *I* can manage to care enough to save that in my DB."
   "Indifference leads to more indifference, and more indifference leads to... I don't really care."
   "Ok. <Btw, help me think of more jokes to say in response to @meh>"
   ])

(def forget-feeling-responses
  ["Forgotten."
   "You've got to hide your @love away. Or @hate, I forgot which."
   "I'm over it if you are."
   "You've lost that lovin' feeling, or possibly that hatin' one."
   ])

(defn forget-feeling
  [person thing]
  (destroy! :feelings {:person      person
                       :thing-lower (string/lower-case thing)}))

(comment
  ; repl interaction area
  ; see all feelings
  (use 'clojure.pprint 'clojure.repl)
  (pprint (map (juxt :person :feeling :thing) (fetch :feelings)))

  (doc update!)

  ; forget all feelings!
  (destroy! :feelings {})

  (def jathom5-feelings (fetch :feelings :where {:person "jathom5"}))
  (def turinturambar-feelings (map #(assoc % :person "turinturambar") jathom5-feelings))

  (doall (map #(update! :feelings %1 %2) jathom5-feelings turinturambar-feelings))

  ; not sure why i can't just do it this way
  (update! :feelings jathom5-feelings turinturambar-feelings :multiple true)


  (def jathom5-karma (fetch-one :karma :where {:nick "jathom5"}))
  (update! :karma jathom5-karma (assoc jathom5-karma :nick "turinturambar"))

  (save-feeling "Bilbo Baggins" :hate
                "chip the glasses and crack the plates")
  (save-feeling "Badri" :love "speaker phones")
  (save-feeling "tomcat" :hate "spaces in path names")
  (save-feeling "bitsbot" :love "brontobot")
  (save-feeling "recursion" :love "recursion")

)


; TODO: make some indices?  m'eh, probably not enough data to matter.
(defn save-feeling
  [person feeling thing]
  (let [thing-lower (string/lower-case thing)]
    (destroy! :feelings {:person      person
                         :feeling     feeling   ; allow love and hate for the same thing?
                         :thing-lower thing-lower})
    (insert! :feelings {:person      person
                        :feeling     feeling
                        :thing       thing
                        :thing-lower thing-lower ; for case-insensitive lookup
                        })))

(defn things-person-feels-about
  "Search your feelings... You know it to be true."
  [person feeling]
  (seq (map :thing
            (fetch :feelings :where {:person  person :feeling feeling}))))

(defn people-who-feel-about-thing
  [thing feeling]
  (seq (map :person
            (fetch :feelings
                   :where {:thing-lower (string/lower-case thing)
                           :feeling     feeling}))))

(defn fetch-random-feeling []
  (when-let [feeling (rand-nth (fetch :feelings))]
    (str (:person feeling)
         (case (:feeling feeling)
           "love" " loves "
           "hate" " hates "
           "meh"  " says m'eh to "
           (str " " (:feeling feeling) " "))
         (:thing feeling))))

(defn fetch-soviet-feeling []
  (when-let [feeling (rand-nth (fetch :feelings))]
    (str "In Soviet Russia, "
         (:thing feeling)
         (case (:feeling feeling)
           "love" " loves "
           "hate" " hates "
           "meh"  " says m'eh to "
           (str " " (:feeling feeling) " "))
         (:person feeling))))

(defn prefix-nick
  [nick & stuff]
  (apply str nick ": " stuff))

(defplugin
  (:cmd
    "profess your love for something. syntax: @love <something you love>"
    #{"love"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (save-feeling nick :love thing)
        (send-message com-m (prefix-nick nick (rand-nth love-responses))))))

  (:cmd
    "express your hatred for something. syntax: @hate <something you hate>"
    #{"hate"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (save-feeling nick :hate thing)
        (send-message com-m (prefix-nick nick (rand-nth hate-responses))))))

  (:cmd
    "express your devastating indifference for something. syntax: @meh <something you don't give a crap about>"
    #{"meh" "indifferent"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (save-feeling nick :meh thing)
        (send-message com-m (prefix-nick nick (rand-nth meh-responses))))))

  (:cmd
    "destroy any trace of your feelings about something. syntax: @forget-feeling <thing to forget about>"
    #{"forget-feeling"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (forget-feeling nick thing)
        (send-message com-m (prefix-nick nick (rand-nth forget-feeling-responses))))))

  (:cmd
    "peer into someone's feelings. syntax: @feelings <person>"
    #{"feelings"}
    (fn [{:keys [nick args] :as com-m}]
      (let [target-nick (or (first args) nick) ; default to the person saying it if no args
            loves       (things-person-feels-about target-nick :love)
            hates       (things-person-feels-about target-nick :hate)
            mehs        (things-person-feels-about target-nick :meh) ]
        (when loves
          (send-item-list com-m (str target-nick " LOVES: ") loves))
        (when hates
          (send-item-list com-m (str target-nick " HATES: ") hates))
        (when mehs
          (send-item-list com-m (str target-nick " says m'eh to: ") mehs))
        (when-not (or loves hates)
          (send-message com-m
                        (str "I am not aware of any feelings on the part of " target-nick "."))))))

  (:cmd
    "see who loves or hates a thing. syntax: @feelings-about <thing>"
    #{"feelings-about" "feelingsabout"
      "who-loves" "wholoves" "who-hates" "whohates"}
    (fn [{:keys [args] :as com-m}]
      (let [thing  (string/join " " args)
            lovers (people-who-feel-about-thing thing :love)
            haters (people-who-feel-about-thing thing :hate) ]
        (when lovers
          (send-message com-m
                        (str thing " is loved by: " (string/join ", " lovers))))
        (when haters
          (send-message com-m
                        (str thing " is hated by: " (string/join ", " haters))))
        (when-not (or lovers haters)
          (send-message com-m
                        (str "Nobody has told me anything about " thing ", but I'm all ears."))))))
    (:cmd
      "display a random feeling by someone about something"
      #{"randfeeling" "random-feeling" "randomfeeling"}
      (fn [{:keys [args] :as com-m}]
        (send-message com-m
                      (if-let [message (fetch-random-feeling)]
                        message
                        "I have no records of feelings by anyone. That makes me feel sad."))
        ))
  (:cmd
    "display a random feeling from the Soviet Union"
    #{"sovietfeeling" "soviet-feeling" "yakov-feeling" "yakovfeeling"}
    (fn [{:keys [args] :as com-m}]
        (send-message com-m
                      (if-let [message (fetch-soviet-feeling)]
                        message
                        "In Soviet Russia, feelings have no records of any bitsbots."))
        )
    )
  )
