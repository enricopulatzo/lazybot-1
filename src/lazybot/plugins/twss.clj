(ns lazybot.plugins.twss
  (:use lazybot.registry)
  (:use [clojure.java.shell :only [sh]])
  (:require [clojure.string :as string])
  )
; TWSS: that's what she said
; could just shell out to this
; https://www.npmjs.org/package/twss


(defn twss-probability [sentence]
  (-> (sh "twss" "--prob" sentence)
      :out
      ))

(comment
  (twss-probability "I'm getting tired of hot bulk loads.")
  )

(defplugin
  (:cmd
    "Show the TWSS-probability of a statement. @twss <phrase goes here>"
    #{"twss"}
    (fn [{:keys [bot args] :as com-m}]
      (let [sentence (string/join " " args)] 
        (send-message
          com-m
          (str "\"" sentence "\": TWSS probability " (twss-probability sentence))
          ))))
  )
