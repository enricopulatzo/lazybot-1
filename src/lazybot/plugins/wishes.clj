(ns lazybot.plugins.wishes
  (:require [clojure.string :as string])
  (:use [lazybot registry]
        [clj-time.core :only [now]]
        [clj-time.coerce :only [to-long]]
        [somnium.congomongo :only [fetch fetch-one insert! destroy!]]))

; commands: 
; @wish <thing with spaces>
; @wishes <user>
; @wishes     # invoke on self
; @wishes-for <thing>
; @forget-wish <thing>
; @randomwish

(comment
  (destroy! :wishes {})
  (->> (fetch :wishes)
       (map :thing)
       )
  
  (count (fetch :feelings))
  (count (fetch :wishes))
  )
(def wish-responses
  ["Wish saved."
   "Sounds good."
   "Wouldn't that be nice?"
   "Your wish is my... database entry."
   "I suggest you wish in one hand, [CENSORED] in the other, and see which mongo schema fills up first."
   "If wishes and buts were clusters of nuts, we'd all have a bowl o' granola!"
   "You people and your wishes..."
   "Wish noted, even though it's silly and maybe you should feel bad for wishing it."
   "You and me both, pal!"
   "If wishes were fishes, the sea would be full. Luckily, though, wishes are just small JSON objects."
   "r If wishes were horses, beggars would ride. If turnips were bayonets, I would wear one by my side. ♫"
   "Wish saved.  But note that I won't be doing a damn thing about it myself."
   "When you wish upon a star... nothing happens. At least when you wish upon a bot, it gets logged somewhere."
   "Gotcha. TODO: build me a servo-controlled nose so I can wiggle it, Bewitched-style."
   ])

(def destroy-wish-responses
  ["wish destroyed. :-/"
   "okay, deleted."
   "I wish you wouldn't do that, but okay."
   ])

(defn save-wish [person thing]
  (let [thing-lower (string/lower-case thing)]
    (insert! :wishes {:person      person
                      :thing       thing
                      :thing-lower thing-lower
                      :timestamp   (to-long (now))
                      :active      true
                      })
    ))

; TODO: keep them but mark them as inactive
(defn destroy-wish [person thing]
  (destroy! :wishes {:person person
                     :thing-lower (.toLowerCase thing)}))

(defn fetch-wishes [person]
  (->> (fetch :wishes :where {:person person})
       (map :thing)
       seq))

(defn fetch-random-wish []
  (when-let [wishes (seq (fetch :wishes))]
    (let [wish (rand-nth wishes)] 
      (str (:person wish) " wishes " (:thing wish)))))

(defn prefix-nick
  [nick & stuff]
  (apply str nick ": " stuff))

(defplugin
  (:cmd
    "express a wish for something.  syntax: @wish <something you wish for>"
    #{"wish"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (save-wish nick thing)
        (send-message com-m (prefix-nick nick (rand-nth wish-responses)))
        )
      ))
  (:cmd
    "forget you wished for something"
    #{"forget-wish" "forgetwish"}
    (fn [{:keys [nick args] :as com-m}]
      (let [thing (string/join " " args)]
        (destroy-wish nick thing)
        (send-message com-m (prefix-nick nick (rand-nth destroy-wish-responses)))
        )
      )
    )
  (:cmd
    "query a person's wishes, defaulting to yourself if unspecified.  syntax: @wishes <person>"
    #{"wishes"}
    (fn [{:keys [nick args] :as com-m}]
      (let [[person & _] args
            person       (or person nick) ]
        (if-let [wishes (fetch-wishes person)]
          (send-message com-m
                        (str person " wishes: " (string/join ", " wishes) "."))
          (send-message com-m (str "Can't find any wishes made by " person "."))))))
  (:cmd
    "display a randomly chosen wish"
    #{"randwish" "random-wish" "randomwish"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m
                    (if-let [message (fetch-random-wish)]
                      message
                      "I have no records about wishes.  I sure wish I did, though."))))
  )
